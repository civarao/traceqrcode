#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/cuda.hpp>
#include <tinyxml2.h>
#include <math.h>
#include <time.h>

using namespace cv;

Point2f computeIntersect(cv::Vec4i a, cv::Vec4i b)
{
    int x1 = a[0], y1 = a[1], x2 = a[2], y2 = a[3];
    int x3 = b[0], y3 = b[1], x4 = b[2], y4 = b[3];

    if (float d = ((float)(x1-x2) * (y3-y4)) - ((y1-y2) * (x3-x4)))
    {
        cv::Point2f pt;
        pt.x = ((x1*y2 - y1*x2) * (x3-x4) - (x1-x2) * (x3*y4 - y3*x4)) / d;
        pt.y = ((x1*y2 - y1*x2) * (y3-y4) - (y1-y2) * (x3*y4 - y3*x4)) / d;
        return pt;
    }
    else
        return cv::Point2f(-1, -1);
}

double computeAngle(cv::Vec4i a, cv::Vec4i b)
{
  double dxa = a[2] - a[0];
  double dya = a[3] - a[1];

  double dxb = b[2] - b[0];
  double dyb = b[3] - b[1];

  double alength = dxa*dxa + dya*dya;
  double blength = dxb*dxb + dyb*dyb;

  if (alength == 0 || blength == 0)
    return -1;

  double result = acos((dxa*dxb + dya*dyb)/(alength*blength));

  return result;
}

typedef struct EndPoint
{
  Point point;
  int la;
  int lb;

  EndPoint(Point point, int la, int lb)
    : point(point), la(la), lb(lb) {}

} EndPoint;

typedef struct MyRect
{
  Point a, b, c, d;


  MyRect(Point a, Point b, Point c, Point d) :
    a(a), b(b), c(c), d(d) {}

  MyRect() :
    a(Point(-1, -1)), b(Point(-1, -1)), c(Point(-1, -1)), d(Point(-1, -1)) {}
} MyRect;

double patWidth = 50;
double patHeight = 50;

void loadPattern(std::vector<Mat>& patterns, const char* file)
{
  Mat pat = imread(file);

  cvtColor(pat, pat, COLOR_BGR2GRAY);

  threshold(pat, pat, 125, 0xff, 1);

  Mat patSmall(Size(patWidth, patHeight), pat.type());

  resize(pat, patSmall, Size(patWidth, patHeight));

  patterns.push_back(patSmall);



}

Mat foundImage;
Mat cannyFrame;
Mat superSmallImage(Size(patWidth, patHeight), CV_8UC1);
Mat warpedImage(Size(patWidth, patHeight), CV_8UC1);
Mat rotMat = getRotationMatrix2D( Point2f(patWidth/2.0, patHeight/2.0), -90, 1 );

void findRectangles(Mat& frame, std::vector<Mat>& rects, std::vector<Mat>& patterns)
{
  Mat smallerImage;

  double widthRatio = 1, heightRatio = 1;
  smallerImage = frame;

  widthRatio = ((double)frame.size().width)/((double)smallerImage.size().width);
  heightRatio = ((double)frame.size().height)/((double)smallerImage.size().height);

  cvtColor(smallerImage, smallerImage, COLOR_BGR2GRAY);

  //Mat superSmallImage(Size(patWidth, patHeight), smallerImage.type());

  const Point2f endPersp[4] = {
    Point2f(0, superSmallImage.size().height),
    Point2f(superSmallImage.size().width, superSmallImage.size().height),
    Point2f(superSmallImage.size().width, 0),
    Point2f(0,0)
  };

  Canny( smallerImage, cannyFrame, 100, 100, 3);

  std::vector<std::vector<Point> > contours;
  findContours(cannyFrame, contours, RETR_LIST, CHAIN_APPROX_SIMPLE);

  std::vector<Point2f> polycont;

  for (int c=0; c<contours.size(); c++)
  {
    const double per = arcLength( contours[c], true);

    polycont.clear();

    approxPolyDP( contours[c], polycont, per*0.02, true);

    if (polycont.size()==4 && isContourConvex(Mat (polycont)))
    {
      Point2f mid = (1.0/4.0)*(polycont[0]+polycont[1]+polycont[2]+polycont[3]);

      Point2f topLeft(-1, -1);
      Point2f topRight(-1, -1);
      Point2f botLeft(-1, -1);
      Point2f botRight(-1, -1);

      for (int i=0; i<4; i++)
      {
        Point2f p = polycont[i];

        bool top = p.y > mid.y;

        if (top)
        {
          if (p.x > topRight.x)
          {
            topLeft = topRight;
            topRight = p;
          }
          else
          {
            topLeft = p;
          }
        }
        else
        {
          if (p.x > botRight.x)
          {
            botLeft = botRight;
            botRight = p;
          }
          else
          {
            botLeft = p;
          }
          
        }
      }

      Point2f startPersp[4] = {topLeft, topRight, botRight, botLeft};

      Mat persTrans = getPerspectiveTransform(startPersp, endPersp);
      warpPerspective(smallerImage, warpedImage, persTrans, warpedImage.size());
      threshold(warpedImage, warpedImage, 125, 0xff, 1);

      for (int pat=0; pat<patterns.size(); pat++)
      {

        Mat superSmallPattern = patterns[pat];

        for (int rot=0; rot<3; rot++)
        {

          Mat xored;

          superSmallImage = warpedImage;
          bitwise_xor(superSmallImage, superSmallPattern, superSmallImage);

          if (countNonZero(superSmallImage) < 600)
          {

            const Point ptArray[4] = {
              Point(polycont[0].x*widthRatio, polycont[0].y*heightRatio),
              Point(polycont[1].x*widthRatio, polycont[1].y*heightRatio),
              Point(polycont[2].x*widthRatio, polycont[2].y*heightRatio),
              Point(polycont[3].x*widthRatio, polycont[3].y*heightRatio)
            };
            const Point * pts = ptArray;
            const int npts = 4;

            polylines(frame, &pts, &npts, 1, true, Scalar(0x00, 0x00, 0xff));

            break;
          }

          //Try rotating it
          warpAffine(warpedImage, warpedImage, rotMat, warpedImage.size());
          warpedImage = warpedImage;
        }
      }

    }
  }

}

int main( int argc, char** argv )
{
  Mat frame;
  std::vector<Mat> rects;

  namedWindow( "result", WINDOW_AUTOSIZE );
  namedWindow( "threshold", WINDOW_AUTOSIZE );

  namedWindow( "result0", WINDOW_AUTOSIZE );
  namedWindow( "result1", WINDOW_AUTOSIZE );
  namedWindow( "result2", WINDOW_AUTOSIZE );
  namedWindow( "result3", WINDOW_AUTOSIZE );

  namedWindow( "pattern", WINDOW_AUTOSIZE );

  double resultYPos = 0;

  std::vector<Mat> patterns;

  const char* qrcodeFile = "qrcode.jpg";

  loadPattern(patterns, qrcodeFile);

  if (argc == 2)
  {
    frame = imread(argv[1]);

    rects.clear();

    findRectangles(frame, rects, patterns);

    imshow("result", frame);

    char windowName[] = "result0";
    std::cout << windowName << std::endl;
    for (int i=0; i<rects.size() && i<4; i++)
    {
      windowName[6] = '0'+i;
      std::cout << windowName << std::endl;
      imshow(windowName, rects[i]);
      moveWindow(windowName, 0, resultYPos);
      resultYPos += rects[i].size().height;
    }

    imshow("pattern", patterns[0]);
    moveWindow("pattern", 500, 0);

    while ( waitKey( 10 ) < 0 );
  }
  else
  {
    VideoCapture capture(0);
    clock_t lastTicks = clock();
    clock_t curTicks;
    double frames = 0;
    double fps = 0;

    while (capture.isOpened())
    {
      capture >> frame;
      if( frame.empty() )
        break;

      rects.clear();

      findRectangles(frame, rects, patterns);

      imshow("result", frame);

      char windowName[] = "result0";
      for (int i=0; i<rects.size() && i<4; i++)
      {
        windowName[6] = '0'+i;
        std::cout << windowName << std::endl;
        imshow(windowName, rects[i]);
        moveWindow(windowName, 0, resultYPos);
        resultYPos += rects[i].size().height;
      }

      imshow("pattern", patterns[0]);
      moveWindow("pattern", 500, 0);

      frames++;
      curTicks = clock();

      if (curTicks - lastTicks >= CLOCKS_PER_SEC)
      {
        fps = CLOCKS_PER_SEC*frames/(curTicks-lastTicks);
        std::cout << "fps: " << fps << std::endl;
        frames = 0;
        lastTicks = curTicks;
      }

      if( waitKey( 10 ) >= 0 )
        break;

      resultYPos = 0;
    }
  }

  destroyWindow("result");

  return 0;

}
