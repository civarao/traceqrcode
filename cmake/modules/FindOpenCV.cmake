# - Try to find OpenCV
# Once done, this will define
#
#  OpenCV_FOUND - system has OpenCV
#  OpenCV_INCLUDE_DIR - the OpenCV include directories
#  OpenCV_LIBRARIES - link these to use OpenCV

# Main include dir
find_path(OpenCV_INCLUDE_DIR
  NAMES opencv.hpp
  PATHS /usr/local/include/opencv2
)

SET (OpenCV_LIB_NAMES
	"calib3d"
	"contrib"
	"core"
	"cuda"
	"cudaarithm"
	"cudabgsegm"
	"cudafeatures2d"
	"cudafilters"
	"cudaimgproc"
	"cudaoptflow"
	"cudastereo"
	"cudawarping"
	"features2d"
	"flann"
	"highgui"
	"imgproc"
	"legacy"
	"ml"
	"nonfree"
	"objdetect"
	"optim"
	"photo"
	"shape"
	"softcascade"
	"stitching"
	"superres"
	"ts"
	"video"
	"videostab"
)

SET(OpenCV_FOUND FALSE)
UNSET (OpenCV_LIBRARIES)

foreach (LIB_NAME ${OpenCV_LIB_NAMES})
	UNSET (LIB_PATH)
	find_library(
		LIB_PATH_${LIB_NAME}
		NAMES "opencv_${LIB_NAME}"
		PATHS ${EXTERNALS_INSTALL_DIR}
	)
	if (LIB_PATH_${LIB_NAME})
		LIST (APPEND OpenCV_LIBRARIES "${LIB_PATH_${LIB_NAME}}" )
		SET(OpenCV_FOUND TRUE)
	else()
		SET(OpenCV_FOUND FALSE)
		break()
	endif()
endforeach()