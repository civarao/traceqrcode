# How to Compile

This project uses the bleeding edge version of OpenCV, which will need to be cloned from the OpenCV git repo. It's a big download. Go to Starbucks.

I'll make a version that works with the standard release of OpenCV soon.

You will need CMake to generate the build files.

The following commands download and install project dependencies and compile the program:

```
#!text

cmake .
make

```

Set the library installation directory by including the EXTERNALS_INSTALL_DIR argument in the call to cmake. Example:

```
#!text

cmake -DEXTERNALS_INSTALL_DIR=/usr/local/lib .

```

# How to Run


Capture images from the camera:
```
#!text

./DisplayImage
```


Use an image file:

```
#!text

./DisplayImage qrcodeRot.jpg
```
